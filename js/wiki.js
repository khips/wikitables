// Lots of stuff up here that's not cross-browser, but this is a browser
// specific extension.

NodeList.prototype.map = Array.prototype.map;
NodeList.prototype.filter = Array.prototype.filter;
NodeList.prototype.forEach = Array.prototype.forEach;

MAGNITUDE_SEPARATOR = ",";
DECIMAL_SEPARATOR = ".";
MAGSEP_RE = (new RegExp("^\\d{1,3}(" + MAGNITUDE_SEPARATOR + "\d\d\d)+$"));
FLOAT_RE = (new RegExp("^\\d+\\" + DECIMAL_SEPARATOR + "\d+$"));

var _ = {
    // Strings:
    is_digit: function(s) {
        return !!s.match(/^\d+$/);
    },

    is_digit_with_magnitude_sep: function(s) {
        // Only recognizes commas as the magnitude separator.
        // TODO: Allow specification of the separator.
        return !!s.match(MAGSEP_RE);
    },

    is_float: function(s) {
        return !!s.match(FLOAT_RE);
    },

    // Types:
    is_string: function(o) {
        return o.constructor == String;
    },

    // Objects:
    extend: function(obj1, obj2) {
        // Simple case of extend, shallow copy.
        for (var attr in obj2) {
            if (!obj2.hasOwnProperty(attr))
                continue;
            obj1[attr] = obj2[attr];
        }
    },

    // Higher order functions:
    comp: function() {
        var fns = Array.prototype.slice.call(arguments).reverse();
        return function(val) {
            fns.forEach(function(fn) {
                val = fn(val);
            });
            return val;
        }
    },

    propget: function(prop) {
        return function(obj) {
            return obj[prop];
        }
    },

    method: function(method) {
        var args = Array.prototype.slice.call(arguments, 1);
        if (args.length)
            return function(obj) {
                return obj[method].apply(obj, args);
            }
        else
            return function(obj) {
                return obj[method]();
            }
    },

    partial: function(fn) {
        var args = Array.prototype.slice.call(arguments, 1);
        return function() {
            var arglist = args.concat(arguments);
            return fn.apply(arglist);
        }
    },

    later: function(fn, cxt, args) {
        return function() {
            return fn.apply(cxt, args);
        }
    },

    lag: function(fn, ms) {
        // Create a function fn2 that, when called, will execute fn() no
        // sooner than _ms_ milliseconds later.  If fn2 is called a second
        // time before that interval has elapsed, fn invocation is
        // rescheduled.
        var timeout;
        return function() {
            clearTimeout(timeout);
            timeout = setTimeout(_.later(fn, this, arguments), ms);
        }
    },

    // DOM:
    offset: function(el) {
        // Calculate the offset of an element
        var leftOS = 0, topOS = 0;
        do {
            leftOS += el.offsetLeft;
            topOS += el.offsetTop;
        } while( (el = el.offsetParent) );

        return {top: topOS, left: leftOS};
    },

    visible: function(el) {
        var style = getComputedStyle(el);
        return style.display !== "none" && style.visbility === "visible";
    },

    closest: function(el, sel) {
        // Finds the closest parent that matches the given CSS
        // selector.
        while (el = el.parentNode)
            if (el.webkitMatchesSelector(sel))
                return el;
    },

    style: function(el, styles) {
        var css = el.style;
        for (var style in styles) {
            if (!styles.hasOwnProperty(style))
                continue;
            css[style] = styles[style];
        }
    },

    set_attrs: function(el, attrs) {
        for (var attr in attrs) {
            if (!attrs.hasOwnProperty(attr))
                continue;
            el.setAttribute(attr, attrs[attr]);
        }
    },

    add_events: function(el, events) {
        for (var etype in events) {
            if (!events.hasOwnProperty(etype))
                continue;
            var evt = events[etype];
            if (typeof evt == "function")
                evt = {handler: evt};
            el.addEventListener(etype, evt.handler, evt.capture || false);
        }
    },

    text: function(el, exclude_match) {
        var pieces = 
            el.childNodes.map(function(node) {
                switch (node.nodeType) {
                    case Node.TEXT_NODE:
                        return node.textContent;
                    case Node.ELEMENT_NODE:
                        if (!_.visible(node))
                            return "";
                        if (!exclude_match || !node.webkitMatchesSelector(exclude_match))
                            return _.text(node);
                    default:
                        return ""
                }
            });
        return pieces.join(" ");
    },

    make: function(spec) {
        // Create a new DOM element.
        if (!spec.tag)
            throw("tag property is required.");

        var el = document.createElement(spec.tag);

        if (spec.style) {
            _.style(el, spec.style);
        }

        var html = spec.content;
        if (html) {
            if (_.is_string(html)) {
                el.innerHTML = html;
            } else if (html.length) {
                html.forEach(function(spec) {
                    if (_.is_string(spec))
                        el.innerHTML += spec;
                    else
                        el.appendChild(_.make(spec));
                });
            } else {
                el.appendChild(_.make(html));
            }
        }

        _.set_attrs(el, spec.attrs);
        _.add_events(el, spec.events);

        return el;
    },

    // Async helper:
    promise: function(fn) {
        var promised = {
            value: undefined,
            on_ready: [],
            on_error: [],
            ready: function(fn) {
                this.on_read.push(fn);
            },
            error: function(why) {
                this.on_error.push(fn);
            }
        };
        var promise = {
            deliver: function(val) {
                promised.value = val;
                promised.on_ready.forEach(function(fn) {
                    fn.call(promised, promised.value);
                });
                promised.on_ready = [];

                promise.deliver = (function() {
                    throw "You have already delivered on that promise.";
                });
            },
            renege: function(err) {
                promised.on_error.forEach(function(fn) {
                    fn.call(promised, err);
                });
            }
        }

        fn(promise);

        return promised;
    }
}


WikiTables = {
    is_wiki_link: function(a) {
        var href = a.getAttribute("href") || "";

        return !!href.match(/\/wiki\//);
    },
    cleanup_contents: function(cell) {
        var ul = cell.querySelector("ul");
        if (ul) {
            var li = ul.querySelectorAll("li");
            var out = li.map(_.cleanup_contents);
            out.multi = true;
            return out;
        }

        var sorttext = cell.querySelector("span.sorttext");
        var contents = sorttext ? sorttext.innerText : _.text(cell, "sup.reference").trim();
        //var contents = sorttext ? sorttext.innerText : _.propget("innerText").text(cell).trim();

        // Detect cell type:
        if (_.is_digit(contents))
            return parseInt(contents);

        // Stuff like 10,000,000
        if (_.is_digit_with_magnitude_sep(contents)) {
            return parseInt(contents.replace(/,/g, ""));
        }

        if (_.is_float(contents)) {
            return parseFloat(contents);
        }

        // Detect entities:
        var links = cell.querySelectorAll("a[title]").filter(WikiTables.is_wiki_link);
        if (links.length) {
            var link = links[0];
            return {title: link.getAttribute("title"), 
                    link: link.getAttribute("href"),
                    resolver: InfoBox.make_resolver_with_link(link),
                    propcache: {}};
        }

        return contents;
    },

    // CSV stuff:
    to_csv_field: function(field) {
        if (field.multi) {
            return field.map(WikiTables.to_csv_field).join("; ");
        }
        field = field.title || field.toString();

        return '"' + field.replace(/"/g, "\\\"") + '"';
    },
    to_csv: function(processed, exclude_columns) {
        // Returns a string representing the processed table data in
        // CSV format.
        var rows = (exclude_columns ? [] : [processed.columns]).concat(processed.rows);

        return rows.map(function(row) {
            return row.map(WikiTables.to_csv_field).join(",");
        }).join("\n");
    },
    make_csv_href: function(processed) {
        return [
            "data:text/csv,",
            encodeURIComponent(WikiTables.to_csv(processed))
        ].join("");
    },
    make_filename: function(tag) {
        var pagetitle = document.title.match(/(.+)- Wikipedia/)[1];

        return [
            tag, "-", pagetitle.slice(0, 25).replace(/\s/g, "_")
        ].join("");
    },

    process_table: function(table, tag) {
        // Convert a table to an Array of Arrays.
        var tbody = table.querySelector("tbody");
        var thead = table.querySelector("thead");

        var column_ths = thead.querySelectorAll("th");
        var column_map = {};
        var columns = [];
        column_ths.forEach(function(th, i) {
            var title = WikiTables.cleanup_contents(th);
            while (column_map[title]) {
                title += "_";
            }
            column_map[title] = i;
            columns.push(title);
        });
        var rows_trs = tbody.querySelectorAll("tr");

        var rows = rows_trs.map(function(tr) {
                        var tds = tr.querySelectorAll("td");
                        var row = tds.map(WikiTables.cleanup_contents);
                        row.tr = tr;    // keeps a reference to <tr>!!
                        return row;
                    });

        var column_cache = {};
        var to_col = function(col) {
            return _.is_string(col) ? column_map[col] : col;
        };
        var get_column = function(col) {
            col = to_col(col);

            if (!col && col !== 0)
                return [];

            column_cache[col] = rows.map(_.propget(col));
            return column_cache[col];
        };
        var get_value = function(col, row) {
            return rows[row] && rows[row][to_col(col)];
        };
        var change_name = function(col, newname) {
            if (column_map[newname])
                throw("That name collides with an existing column name.");

            col = to_col(col);
            var oldname = columns[col];
            columns[col] = newname;
            column_map[newname] = col;
            delete column_map[oldname];
        };
        var has_column = function(colname) {
            return !!column_map[colname];
        };

        return {"columns":      columns,
                "rows":         rows,
                "tag":          tag || "untitled",
                "get_column":   get_column,
                "get_value":    get_value,
                "change_name":  change_name,
                "has_column":   has_column};
    },

    find_tables: function(scope)  {
        scope = scope || window.document;

        return scope.querySelectorAll("table.wikitable");
    },

    _on_download_click: function(e) {
        // Clicked on the download link.
    },

    _on_formula_change: function(table, processed, e) {
        console.log("Loading dependencies.");
        var target = e.currentTarget;
        var value = target.value;

        try {
            var parse_tree = Parser.parse_str(value);
        } catch(error) {
            var error_div = table.querySelector("div.wt_error");
            if (error instanceof Parser.Error)
                error_div.innerText = "Parser error: invalid syntax.";
            else if (error instanceof Tokenizer.Error)
                error_div.innerText = error.msg;
            else
                throw(error);

            return;
        }

        var cxt = new Context();
    },

    _on_add_column: function(e) {
        var table = _.closest(e.currentTarget, "table");
        console.log(table);
        var col_heads = table.querySelector("thead tr:last-child");
        var newhead = _.make({
            "tag": "th",
            "attrs": { "class": "wt_calcrow" },
            "content": [{ "tag": "textarea",
                          "events": { "keyup": _.partial(WikiTables._on_formula_change, table, null) } },
                        { "tag": "div",
                          "class": "wt_error" }] });

        col_heads.appendChild(newhead);

        var rows = table.querySelectorAll("tbody tr");
        rows.forEach(function(tr) {
            tr.appendChild(_.make({ "tag": "td" }));
        });

        e.preventDefault();
    },

    setup_table: function(table) {
        // Sets up a table so that it can be exported.
        // Create some controls:
        var thead = table.querySelector("thead");
        var first_tr = thead.querySelector("tr");

        var processed = WikiTables.process_table(table, "table");

        var control_tr = 
        _.make({
            tag: "tr",
            content: {
                "tag": "th",
                "attrs": { "colspan":  processed.columns.length, 
                           "class":    "supertable"},
                "style": {"height": "auto"},
                "content": [
                    {   "tag": "a",
                        "content": "Download CSV",
                        "attrs": { "title":    "Download as CSV",
                                   "download": WikiTables.make_filename(processed.tag),
                                   "href":     WikiTables.make_csv_href(processed)} },

                    "&nbsp;&nbsp;&bull;&nbsp;&nbsp;",
                    {   "tag": "a",
                        "content": "Chart",
                        "events": {
                            "click": Wikitables.do_chart }},
                    "&nbsp;&nbsp;&bull;&nbsp;&nbsp;",

                    {   "tag": "a",
                        "content": "Add Column",
                        "events": {
                            "click": WikiTables._on_add_column
                        },
                        "attrs": {
                            "href": "#",
                            "title": "Add a Column"  } } ] } });

        thead.insertBefore(control_tr, first_tr);
    },

    _on_mouseover: function(e) {
        var table = e.currentTarget;
        WikiTables.setup_table(table);

        table.removeEventListener("mouseover", WikiTables._on_mouseover, false);
    },

    setup: function() {
        // Initial setup.
        var tables = WikiTables.find_tables();
        // Will change how this works later...
        tables.forEach(function(table) {
            table.addEventListener("mouseover", WikiTables._on_mouseover, false);
        });
    },

    create_sketch: function() {
        // Create a new region for drawing content.
        var box = _.make({  tag: "div",
                            attrs: { "class": "wt_sketch_container" },
                            style: {    position: "fixed",
                                        top: "10px",
                                        right: "10px" },
                            content: [
                                { tag: "div",
                                  attrs: {"class": "wt_sketch_head"}},
                                { tag: "svg", 
                                  attrs: {"class": "wt_sketch_content"}}
                            ]});
        document.body.appendChild(box);
        return box.querySelector("svg");
    }
};

WikiTables.TableData = (function(data) {
    this.data = data;
});

_.extend(WikiTables.TableData.prototype, {
    // Perhaps swap in a SQLite database on enabled browsers here.
    get_column: function(name) {
        // return this.table
    },
    
    get_value: function(column, row_num) {
        // return the value in row row_num in column column.
        return this.table[column] && this.table[column][row_num];
    },
    
    change_name: function(name, newname) {
        if (newname in this.table) {
            throw("name is used");
        }
        
        this.table[name] = this.table[current];
        delete this.table[current];
    },
    
    has_column: function(name) {
        return !!(name in this.table);
    },
    
    unused_column_name: function() {
        return "undefined" + this.columns;
    }
});

WikiTables.setup();
WikiTables.svg = WikiTables.create_sketch();

d3.svg