// Tokenizer.js
// BDS, 2009
// Tokenize the input, outputting an array of token objects, the structure of
// which is documented below.
//
// This is really old, but I never did much with it, so I'll use it
// until I get the time and inclination to rewrite.

var Tokenizer = {
    // Significant tokens:
    //  " : denotes the beginning and end of a string literal
    //  ; : begin comment
    //  % :
    //  + :
    //  - :
    //  / :
    //  * : Arithmetic operators
    //  & : concat operator
    //  ( : begin expression or call
    //  ) : finish expression or call
    //  \ : escape the following character (in strings and identifiers)
    //  : : condition
    //
    //  The full language grammar is described in parser.js.
    //  

    num_re: /^(\d+(\.\d+)?(e-?\d+)?|(#|0x)([0-9a-f]+))$/i,
    binding_re: /^[a-z_][a-z_0-9]*$/i,
    
    // Tokenizer states //////
    // TODO: $preprocess_constants Python preprocessor should insert
    //  constant values like tokenizer states.
    TK_NO_STATE: 0,
    TK_STR_STATE: 1,
    TK_CMT_STATE: 2,
    TK_IDENT_STATE: 3,
    //////////////////////////
    
    Error: function(msg) {
        // Tokenizer error.
        this.msg = msg;
    },
    
    tokenize: function(s) {
        // Given a string, produces a stream of corresponding tokens
        // Tokens are objects with this form:
        //  {
        //      token: <integer value>,
        //      value: <optional payload>,
        //      offset: <index in input string where the token ends>
        //  }
        var acc = "",
            chr,
            i = 0,
            state = 0,
            escaped = false,
            tokens = [],
            last_newline = 0,
            stopchar;
        
        function push_literal() {
            if (!acc) return;
            var t = {
                offset: i
            }, m;
            /*if (Tokenizer.keywords[acc]) {
                t.token = Tokenizer.keywords[acc];
            } else */
            if (m = Tokenizer.num_re.exec(acc)) {
                t.token = Tokenizer.t.NUM_LIT;
                t.value = m[4] ? parseInt("0x" + m[5]) : parseFloat(acc);
            } else if (Tokenizer.binding_re.exec(acc)) {
                t.token = Tokenizer.t.IDENT;
                t.value = acc;
            } else {
                throw(new Tokenizer.Error("Invalid literal: \"" + acc + "\""));
            }
            tokens.push(t);
            acc = "";
        }

        tok = Tokenizer.t;

        while (chr = s[i++]) {
            if (state) {
                if (state == Tokenizer.TK_STR_STATE ||
                    state == Tokenizer.TK_IDENT_STATE) {
                    if (chr == stopchar && !escaped) {
                        var t = state == Tokenizer.TK_STR_STATE ? tok.STR_LIT : tok.IDENT;
                        if (tokens[tokens.length-1] && 
                            t == tok.STR_LIT &&
                            tokens[tokens.length-1].token == t) {
                            // concatenate adjacent strings
                            var last_tok = tokens[tokens.length-1];
                            last_tok.value += acc;
                        } else {
                            tokens.push({
                                token: t,
                                value: acc,
                                offset: i + stopchar.length
                            });
                        };
                        acc = "";
                        state = Tokenizer.TK_NO_STATE;
                    } else if (chr == "\\") {
                        escaped = true;
                    } else {
                        escaped = false;
                        if (chr != "\n") {
                            acc += chr;
                        }
                    }
                } else if (state == Tokenizer.TK_CMT_STATE && chr == "\n") {
                    // Comments continue from the \ to the end of the line
                    state = Tokenizer.TK_NO_STATE;
                }
            } else {
                t = {};
                if (Tokenizer.token_map[chr]) {
                    push_literal();
                    t.token = Tokenizer.token_map[chr];
                } else if (chr == '"') {
                    // string literal
                    stopchar = '"';
                    state = Tokenizer.TK_STR_STATE;
                    continue;
                } else if (chr == ';') {
                    // comment
                    state = Tokenizer.TK_CMT_STATE;
                    continue;
                } else if (chr == "\n" || chr == "\t" || chr == ' ') {
                    // whitespace; ignore
                    push_literal();
                    if (chr == "\n" && s[i-2] != "\\") {
                        last_newline = tokens.length;
                    }
                    continue;
                } else if (chr == "'") {
                    stopchar = "'";
                    state = Tokenizer.TK_IDENT_STATE;
                    continue;
                } else if (chr == ":") {
                    push_literal();
                    tokens.splice(last_newline, 0, {
                        token: tok.BEGIN_CONDITION
                    });
                    t.token = tok.END_CONDITION;
                } else if (chr == ">") {
                    push_literal();
                    if (s[i] == "=") {
                        ++i;
                        t.token = tok.GE;
                    } else {
                        t.token = tok.GT;
                    }
                } else if (chr == "<") {
                    push_literal();
                    if (s[i] == "=") {
                        ++i
                        t.token = tok.LE;
                    } else {
                        t.token = tok.LT;
                    }
                } else if (chr != "\\") {
                    acc += chr;
                    continue;
                }
                
                t.offset = i;
                tokens.push(t);
                escaped = false;
            }
        }

        switch (state) {
            case Tokenizer.TK_STR_STATE:
                throw(Tokenizer.Error("Unterminated string literal."));
            default:
        }

        push_literal();

        return tokens;
    },

    print_tokens: function(token_list) {
        var s = [],
            i = 0,
            token;
        
        while(token = token_list[i++]) {
            s.push(Tokenizer.name_map[token.token]);
            if (token.value) {
                s.push(": ");
                s.push(token.value.toString());
            }
            s.push("\n");
        }

        return s.join("");
    },
    
    replace_tokens: function(formula, tokens, indexes, new_value) {
        var pieces = [],
            last_position = 0;
        
        for (var i = 0, l = indexes.length; i < l; ++i) {
            var last = tokens[indexes[i]-1];
            pieces.push(formula.slice(last_position, last ? last.offset : 0));
            pieces.push(new_value);
            
            last_position = tokens[indexes[i]].offset - 1;
        }
        
        pieces.push(formula.slice(last_position, -1));
        
        return pieces.join("");
    },
    
    replace_identifiers: function(text, name, new_name) {
        // Replaces occurrences of identifiers matching 'name' in the original
        // formula text with new_name and returns the new formula.
        // Higher level version of Tokenizer.replace_tokens().
        
        var tokens = Tokenizer.tokenize(text),
            replace = [];
        
        new_name = "'" + new_name.replace("'", "\\'") + "'";
        
        for (var i = 0, l = tokens.length; i < l; ++i) {
            if (tokens[i].token == Tokenizer.t.IDENT &&
                tokens[i].value == name) {
                replace.push(i);
            }
        }
        
        return Tokenizer.replace_tokens(text, tokens, replace, new_name);
    }
};

Tokenizer.token_map = {
// fixed width tokens:
    "(": "OPEN_PAREN",
    ")": "CLOSE_PAREN",
    "{": "OPEN_BRACE",
    "}": "CLOSE_BRACE",
    "+": "ADD_OP",
    "-": "SUBT_OP",
    "&": "CONCAT_OP",
    "/": "DIV_OP",
    "*": "MUL_OP",
    "%": "MOD_OP",
    ",": "ARG_SEPARATOR",
// variable width tokens (keyed with integers):
    0: "IDENT",
    1: "NUM_LIT", 
    2: "STR_LIT",
    3: "BEGIN_CONDITION",
    4: "END_CONDITION",
    5: "AND",
    6: "OR",
    7: "NOT",
    8: "GT",
    9: "GE",
    10: "LT",
    11: "LE"
//////////////////
}

Tokenizer.t = {};
Tokenizer.name_map = {};

var i = 0;
for (var t in Tokenizer.token_map) {
    Tokenizer.t[Tokenizer.token_map[t]] = ++i;
    Tokenizer.name_map[i] = Tokenizer.token_map[t];
    if (parseInt(t) >= 0) {
        delete Tokenizer.token_map[t];
    } else {
        Tokenizer.token_map[t] = i;
    }
}
delete i, t;

//Tokenizer.keywords = {
//    "and": Tokenizer.t.AND,
//    "or": Tokenizer.t.OR,
//    "not": Tokenizer.t.NOT
//}