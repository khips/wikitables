// Evaluator.js
// BDS, 2009
// Takes a parse tree from parser.js and evaluates it.

if (!window.Tokenizer) {
    throw("Tokenizer must be imported before Parser.");
}

var tokens = Tokenizer.t;

// RuntimeError constructor.
function RuntimeError(msg) {
    this.msg = msg;
}

FnMap = {
    // When running, all functions are bound to a Context object
    // that handles variables, etc.
    "sum": function(list) {
        list = this.evaluate(list, Context.LIST_CONTEXT);

        var total = 0;

        for (var i = 0, l = list.length; i < l; ++i) {
            total += parseFloat(list[i]);
        }
        
        return total;

    },
    "add": function() { 
        var total = 0,
            value;

        for (var i = 0, l = arguments.length; i < l; ++i) {
            value = parseFloat(this.evaluate(arguments[i]));
            if (isNaN(value)) {
                value = 0;
            }
            total += value;
        }
        
        return total;
    },
    "subt": function() {
        var i = 1,
            acc = this.evaluate(arguments[0]);

        for (var l = arguments.length; i < l; ++i) {
            acc -= this.evaluate(arguments[i]);
        }

        return acc;
    },
    "mul": function() {
        var acc = 1;

        for (var i = 0, l = arguments.length; i < l; ++i) {
            acc *= this.evaluate(arguments[i]);
        }

        return acc;
    },
    "div": function() {
        if (arguments.length < 2) {
            throw(new RuntimeError("Must have at least two arguments."));
        }
        
        var acc = this.evaluate(arguments[0]);

        for (var i = 1, l = arguments.length; i < l; ++i) {
            acc /= this.evaluate(arguments[i]);
        }

        return acc;
    },
    "mod": function() {
        if (arguments.length < 2) {
            throw(new RuntimeError("Must have at least two arguments."));
        }

        var result = this.evaluate(arguments[0]);

        for (var i = 1, l = arguments.length; i < l; ++i) {
            result %= this.evaluate(arguments[i]);
        }

        return result;
    },
    "strcat": function() {
        var output = "";

        for (var i = 0, l = arguments.length; i < l; ++i) {
            output += this.evaluate(arguments[i]).toString();
        }

        return output;
    },

    "commas": function(num, join) {
        join = join || ",";
        num = this.evaluate(num).toString();

        var pieces = [];
        
        while (num.length > 3) {
            pieces.unshift(num.slice(-3));
            num = num.slice(0, -3);
        };

        pieces.unshift(num);

        return pieces.join(join);
    },

    "if": function(cond, iftrue, iffalse) {
        if (this.evaluate(cond)) {
            return this.evaluate(iftrue);
        } else if (iffalse) {
            return this.evaluate(iffalse);
        }
    }, 
    "eq": function(first, second) {
        return (this.evaluate(first) == this.evaluate(second)) ?
            1 : 0;
    },
    "gt": function(first, second) {
        return (this.evaluate(first) > this.evaluate(second)) ?
            1 : 0;
    },
    "lt": function(first, second) {
        return (this.evaluate(first) < this.evaluate(second)) ?
            1 : 0;
    },
    "ge": function(first, second) {
        return (this.evaluate(first) >= this.evaluate(second)) ?
            1 : 0;
    },
    "le": function(first, second) {
        return (this.evaluate(first) <= this.evaluate(second)) ?
            1 : 0;
    },

    "not": function(tree) {
        return (this.evaluate(tree)) ? 0 : 1;
    },
    
    // 'and' and 'or' use short-circuit evaluation
    "and": function() {
        for (var i = 0, l = arguments.length; i < l; ++i) {
            if (!this.evaluate(arguments[i])) {
                return false;
            }
        }

        return true;
    },
    "or": function() {
        for (var i = 0, l = arguments.length; i < l; ++i) {
            if (this.evaluate.arguments[i]) {
                return true;
            }
        }

        return false;
    },

    "let": function(ident, value, tree) {
        if (ident.token != tokens.IDENT) {
            // TODO: $let1 Throw a proper error here.
            throw("Type error: ");
        } 

        this.set_variable(ident.value, this.evaluate(value));
        var ret = this.evaluate(tree);
        this.rollback_variable(ident.value);

        return ret;
    },

    "set": function(ident, new_value) {
        if (ident.token != tokens.IDENT) {
            throw("Type error: ");
        }

        new_value = this.evaluate(new_value);
        ident = ident.value;
        
        if (Setter[ident]) {
            Setter[ident].call(this, new_value);
        } else {
            new_value = this.evaluate(new_value);
            this.reset_variable(ident, new_value);
        }

        return new_value;
    },

    "defined": function(ident) {
        // return true if the given value is defined.

        return !!this.globals[ident.value];
    },
    "resolver": function(name) {
        // Get a value by name.
        return this.resolve(this.evaluate(name), this.last_context);
    },

    "pow": function(value, pow) {
        return Math.pow(
            this.evaluate(value),
            this.evaluate(pow));
    },

    "square": function(value) {
        FnMap.pow(value, 2);
    },

    "round": function(value, places) {
        if (places) {
            return this.evaluate(value).toFixed(parseInt(this.evaluate(places)));
        } else {
            return Math.round(
                this.evaluate(value));
        }
    },

    "ceil": function(value) {
        return Math.ceil(
            this.evaluate(value));
    },

    "rand": function(up_to) {
        up_to = this.evaluate(up_to);
        return Math.floor(Math.random() * up_to + 1);
    },

    "floor": function(value) {
        return Math.floor(
            this.evaluate(value));
    },

    "avg": function(list) {
        // Average the numbers in the list.
        list = this.evaluate(list, Context.LIST_CONTEXT);
        var total = 0;

        for (var i = 0, l = list.length; i < l; ++i) {
            total += parseFloat(list[i]);
        }
        
        return total/list.length;
    },

    "stddev": function(list) {
        list = this.evaluate(list, Context.LIST_CONTEXT);

        
    },

    "range": function(start, end) {
        // Return a list of numbers from start to end.
        start = this.evaluate(start);
        end = this.evaluate(end);

        if (typeof start != "number") {
            throw("Type error: ");
        }
        if (typeof end != "number") {
            throw("Type error: ");
        }
        
        var out = [];

        for (var i = start; i <= end; i++) {
            out.push(i);
        }

        return out;
    },
    
    "map": function(ident, list, expr) {
        list = this.evaluate(list, Context.LIST_CONTEXT);
        
        if (list.length == undefined) {
            throw("Type error: ");
        }
        var bind = ident.value,
            out = Parser.mklist();

        this.set_variable(bind, 0);

        for(var i = 0, l = list.length; i < l; ++i) {
            this.reset_variable(bind, list[i]);
            
            out.push(this.evaluate(expr));
        }
        
        this.rollback_variable(bind);

        return out;
    },

    "multimap": function(idents, lists, expr) {
        if (!idents.list) {
            throw("");
        }
        if (!lists.list) {
            throw("");
        }

        var list_of_lists = [], out = [];
        
        for (var i = 0, l = lists.length; i < l; ++i) {
            list_of_lists.push(
                this.evaluate(lists[i], Context.LIST_CONTEXT));
        }

        for (var i2 = 0, l2 = idents.length; i2 < l2; ++i2) {
            this.set_variable(idents[i2].value, null); 
        }

        try {
            for (var i = 0, l = list_of_lists[0].length; i < l; i++) {
                for (var i2 = 0, l2 = idents.length; i2 < l2; ++i2) {
                    this.reset_variable(idents[i2].value, list_of_lists[i2][i]);
                }
                out.push(this.evaluate(expr));
            }
        } catch(err) {
            if (list_of_lists.length != idents.length) {
                throw(
                    new RuntimeError(
                        "Not enough identifiers."));
            }
        }

        return out;
    },

    "foreach": function(ident, list, expr) {
        list = this.evaluate(list, Context.LIST_CONTEXT);

        if (list.length == undefined) {
            throw(new RuntimeError("Type error"));
        }

        var bind = ident.value;

        this.set_variable(bind, 0);

        for (var i = 0, l = list.length; i < l; ++i) {
            this.reset_variable(bind, list[i]);
        }

        return "";
    },
    
    "fold": function(ident, acc_ident, list, expr) {
        // that's accumulator identifier; not an accident
        var name = ident.value,
            name2 = acc_ident.value;
        
        list = this.evaluate(list, Context.LIST_CONTEXT);

        this.set_variable(name2, list[0]);

        for(var i = 1, l = list.length; i < l; ++i) {
            this.reset_variable(name, list[i]);

            this.reset_variable(name2, this.evaluate(expr));
        }

        var ret = this.globals[name2];

        this.rollback_variable(name);
        this.rollback_variable(name2);

        return ret;
    },

    "slice": function(list, start, end) {
        list = this.evaluate(list, Context.LIST_CONTEXT);
        start = this.evaluate(start);

        if (typeof end == "undefined") {
            end = -1;
        } else {
            end = this.evaluate(end);
        }
        
        // TODO: $fn_slice Finish slice implementation.
    },

    "split": function(str, splitter, count) {
        str = this.evaluate(str).toString();
        splitter = this.evaluate(splitter).toString();
        
        return (count) ?
            str.split(splitter, parseInt(this.evaluate(count))) :
            str.split(splitter);
    },

    "join": function(list, delimiter) {
        list = this.evaluate(list, Context.LIST_CONTEXT);
        
        return list.join(this.evaluate(delimiter));
    },

    "count": function(list) {
        return this.evaluate(list, Context.LIST_CONTEXT).length;
    },
    
    // date functions
    "date": function(datestr) {
        // TODO: $dateparse Tie this into the Date parser
        return Date.parse(datestr);
    },
    "date_range": function(datestr) {
        // TODO: $daterangeparse Date range representation.
    },
    "parse_pretty": function(value) {
        
    },
    "smartcast": function(str) {
        // Smartcast attempts to detect the type of the value being parsed and
        // then converts to that type.
        
        // TODO: $smartcast Function for intelligent value parsing.
    },

    "cell_color": function(cell, color) {
        return FnPattern.set_style.call(
                this, arguments[0], "color", arguments[1]);
    },
    "cell_bg": function(cell, color) {
        return FnPattern.set_style.call(
                this, arguments[0], "backgroundColor", arguments[1]);
    },
    "bold": function(cell) {
        return FnPattern.set_style.call(
                this, arguments[0], "font-weight", "bold");
    },
    "italicize": function(cell) {
        return FnPattern.set_style.call(
                this, arguments[0], "font-stye", "italic");
    },

    "svg_color": function(element_id, color) {
        var doc = document.getElementById("diagram").getSVGDocument();

        if (!doc) {

        }

        var element = doc.getElementById(this.evaluate(element_id));

        if (!element) {

        }

        element.setAttribute('fill', this.evaluate(color));

        return "";
    },
    "max": function(list) {
        if (arguments.length > 1) {
            list = arguments;
        }
        
        list = this.evaluate(list, Context.LIST_CONTEXT);
        
        var max = list[0];

        for (var i = 1, l = list.length; i < l; ++i) {
            if (list[i] > max) {
                max = list[i];
            }
        }

        return max;
    },
    "min": function(list) {
        if (arguments.length > 1) {
            list = arguments;
        }
        
        list = this.evaluate(list, Context.LIST_CONTEXT);
        
        var min = list[0];

        for (var i = 1, l = list.length; i < l; ++i) {
            if (list[i] < min) {
                min = list[i];
            }
        }

        return min;
    },

    "to_color": function(num) {
        var color = Math.round(num).toString(16);
        
        return "#" + "000000".slice(0, 6 - color.length) + color;
    },

    "color_range": function(min, min_color, max, max_color, value) {
        value = this.evaluate(value);
        min = this.evaluate(min);
        var min_code = parseInt(this.evaluate(min_color));

        if (isNaN(min_code)) {
            min_code = Context.Colors[min_color];
            if (min_color == undefined) {
                throw(
                    new RuntimeError());
            }
        }

        if (value <= min) {
            return FnMap.to_color(min_code);
        }

        max = this.evaluate(max);
        var max_code = parseInt(this.evaluate(max_color));

        if (isNaN(max_code)) {
            max_code = Context.Colors[max_color];
            if (max_code == undefined) {
                throw(
                    new RuntimeError());
            }
        }

        if (value >= max) {
            return FnMap.to_color(max_code);
        }

        var scale = (value - min)/(max - min),
            red = (min_code>>16)+(((max_code>>16)-(min_code>>16))*scale),
            green = (min_code>>8&255)+(((max_code>>8&255)-(min_code>>8&255))*scale),
            blue = (min_code&255)+(((max_code&255)-(min_code&255))*scale);

        return FnMap.to_color((red<<16) + (green<<8) + blue);
    },
    
    "where": function(condition, expression) {
        // Return the evaluated _expression_ in the context of the table row 
        // where condition evaluates to true.  An extremely simplistic query 
        // tool.
        // Returns a null string if there is no match.
        var old_row = this.get_row();
        
        try {
            var i = this.find_first(condition);
        } catch(err) {
            if (err == Context.NO_MATCH) {
                // The condition did not match any row.
                return "";
            }
            
            // rethrow the error.
            throw(err);
        }
        
        var out = this.evaluate(expression);
        
        // Restore the previous value of the current row.
        this.set_row(old_row);
        
        return out;
    },
    
    "lookup": function(expression, match_expr, ret_expr) {
        // Return the evaluation of ret_expr in the context of rows where the
        // value of expression matches the value of match_expr as evaluted in
        // the current context.
        // A simple example of the utility of this function is to perform
        // lookups.  lookup(State, "MD", )
        
        // store the current state information
        var get_list = this.last_context = Context.LIST_CONTEXT,
            match = this.evaluate(match_expr),
            cell = this.get_current_cell(),
            row = this.get_row();

        // There is no current cell.  It doesn't make sense in this context.
        this.current_cell = null;
        this.disable_side_effects();
        
        var values = [];
        
        for (var i = 0, l = this.table.rows; i < l; ++i) {
            this.set_row(i);
            if (this.evaluate(expression)) {
                values.push(this.evaluate(ret_expr));
                if (!get_list) {
                    break;
                }
            }
        }

        if (!values.length) {
            // Got to the end of the loop without finding anything.
            throw(Context.NO_MATCH);
        }
        
        // restore state
        this.set_row(row);
        this.enable_side_effects();
        this.current_cell = cell;
    },
    
    "row": function() {
        return this.get_row()+1;
    }
}

// Aliases
FnMap.cell_background = FnMap.cell_bg;
FnMap.average = FnMap.avg;

var FnPattern = {
    set_style: function(cell, style, value) {
        if (value) {
            cell = this.evaluate(cell, Context.CELL_CONTEXT);
            value = this.evaluate(value);
        } else {
            value = this.evaluate(cell);
            cell = this.get_current_cell();
        }

        if (cell.element) {
            cell.element.style[style] = value;
        } else {
            throw(
                new RuntimeError("There is no associated cell."))
        }

        return "";
    }
}

var Setter = {
    "this": function(value) {
        this.get_current_cell().value = value;
    }
}

for (name in FnMap) {
    // Allow functions to be identified by name.
    FnMap[name].name = name;
}

// Functions specifically marked non-static.
FnMap.rand.nonstatic = true;
FnMap.row.nonstatic = true;
// Functions with side-effects.
FnMap.svg_color.nonstatic = true;
FnMap.cell_bg.nonstatic = true;

// Currently, where() is always static...
FnMap.where.always_static = true;

OperatorFn = {};
OperatorFn[tokens.MUL_OP] = FnMap.mul;
OperatorFn[tokens.DIV_OP] = FnMap.div;
OperatorFn[tokens.ADD_OP] = FnMap.add;
OperatorFn[tokens.SUBT_OP] = FnMap.subt;
OperatorFn[tokens.CONCAT_OP] = FnMap.strcat;
OperatorFn[tokens.MOD_OP] = FnMap.mod;
OperatorFn[tokens.GE] = FnMap.ge;
OperatorFn[tokens.GT] = FnMap.gt;
OperatorFn[tokens.LT] = FnMap.lt;
OperatorFn[tokens.LE] = FnMap.le;

function BasicResolver(table) {
    this.table = table;
    this.column_data = [];
}

BasicResolver.prototype.resolve = function(cxt, name) {
    // TODO: $resolver Table column resolver
    return null;
}


function Context(table, globals, optimizations_off) {
  // execution context
  // Manages scopes and related stuff.
  //
  // Parameters:
  //    table: An instance of Evaluator.TableData, or compatible.
  //    globals: A dictionary containing name bindings.  A shallow _copy_ of
  //        this dictionary will be made, so modifications to the object passed
  //        will not be reflected in the context.
  //    optimizations_off: If true, the evaluator will not attempt to optimize
  //        the parse tree on its first run.  (For debugging, mostly, or perhaps
  //        for conditions where typically static variables may be volatile.)
  
  this.table = table;
  this.globals = {};
  
  if (globals) {
      _.extend(this.globals, globals);
  }
  
  this.last_value = 0;
  
  if (!optimizations_off) {
      this.original_evaluate = this.evaluate;
      this.evaluate = this.optimizing_evaluate;
  }
}

// Resolver contexts; see more in Context#evaluate().
Context.NORMAL_CONTEXT = 1;
Context.LIST_CONTEXT = 2;
Context.SVG_CONTEXT = 3;
Context.CELL_CONTEXT = 4;

Context.NO_MATCH = {};

_.extend(Context.prototype, {
    resolve: function(ident, rcontext) {
        // Parameters:
        //  ident: Identifier to resolve
        //  rcontext: See documentation in Context#evaluate().
        //
        //  TODO: $context_caching Cache all identifiers that can be
        //      definitively determined not to vary throughout the
        //      evaluation context.
        var name = ident.value || ident.toString();
        
        if (name == "last") {
            return this.last_value;
        }

        var resolved = this.globals[name];
        
        if (resolved != undefined) {
            return resolved[0];
        }
        
        // Couldn't be resolved using the variables.
        // Check the table.
        
        // If the resolver context is a list (for example, if the identifier
        // being resolved is the 'list' argument of list processing function),
        // generate the appropriate list by 
        if (rcontext == Context.LIST_CONTEXT) {
            // TODO: $listcxt_eff Make this more efficient.
            var column = this.table.get_column(name),
                cells = [];
                
            for (var i = 0, l = column.length; i < l; i++) {
                if (this.current_cell) {
                    Evaluator.add_dependent(column[i], this.current_cell);
                }
                cells.push(column[i].value);
            }

            cells.is_static = true;
            
            return cells;
        }

        var cell = this.table.get_value(name, this.get_row()) || {};

        if (rcontext == Context.CELL_CONTEXT) {
            return cell;
        }
        
        if (this.current_cell) {
            Evaluator.add_dependent(cell, this.current_cell);
        }
        
        if (cell.value == undefined) {
            throw(
                new RuntimeError("Unrecognized word: \"" + name + "\""));
        }

        return cell.value;
    },
    
    evaluate: function(tree, context) {
        // Parameters:
        //  tree: parse subtree to process
        //  context: confusingly named, this optional parameter is an
        //      optional instruction to the resolver.  For example, if a
        //      resolver is instructed to 

        // TODO: $analyze Collect certain information about the expression,
        //  including how many references it has that can be

        if (tree && tree.list) {
            var copy = [];
            for (var i = 0, l = tree.length; i < l; ++i) {
                copy[i] = this.evaluate(tree[i]);
            }
            return (this.last_value = copy);
        }

        if (!tree) {
            return tree;
        }
        
        var out;

        if (tree.fn) {
            var old_context = this.last_context;
            this.last_context = context || this.last_context;

            out = tree.fn.apply(this, tree.children);

            this.last_context = old_context;

            if (!tree.fn.nonstatic && this.optimize_static_subtrees) {
                if (!tree.fn.always_static) {
                    for (var i = 0, l = tree.children.length; i < l; ++i) {
                        if (!tree.children[i].is_static) {
                            return this.last_value = out;
                        }
                    }
                }
                // all the children were marked as static.
                delete tree.fn
                delete tree.children;
                tree.value = out;
                tree.is_static = true;
            }
        } else if (tree.token == tokens.IDENT) {
            out = this.resolve(tree, context);
            if (this.optimize_static_subtrees && out.is_static) {
                delete tree.token;
                tree.value = out;
                tree.is_static = true;
            }
        } else if (tree.value) {
            out = tree.value;
        } else {
            out = tree;
        }
        return this.last_value = out;
    },

    set_variable: function(name, value) {
        // Put the value associated with the name on the top of
        // an array.
        if (this.globals[name]) {
            this.globals[name].unshift(value);
        } else {
            this.globals[name] = [value];
        }
    },
    
    optimizing_evaluate: function(tree, context) {
        // Run only for the first tree evaluated, this assists in performing
        // certain optimizations on the parse tree.  The existing tree is
        // modified, so subsequent evaluations on that tree will actually be
        // performed with an optimized tree (with branches potentially removed)
        
        this.optimize_static_subtrees = true;
        
        // track variable usage?
        // this.original_set_variable = this.set_variable;
        // this.original_reset_variable = this.reset_variable;
        // this.set_variable = this.optimizing_set_variable;
        // this.reset_variable = this.optimizing_reset_variable;
        
        // This will run for all future invocations of evaluate, including
        // those that happen below the original_evaluate() 
        this.evaluate = this.original_evaluate;

        var out = this.original_evaluate(tree, context);
        
        this.optimize_static_subtrees = false;
        
        // this.set_variable = this.original_set_variable;
        // this.reset_variable = this.original_reset_variable;
        // 
        // delete this.original_set_variable;
        // delete this.original_reset_variable;
        
        return out;
    },
    
    optimizing_set_variable: function(name, value) {
        
        
        this.original_set_variable(name, value);
    },

    reset_variable: function(name, value) {
        // Reset the value of the variable for the current scope.
        if (!this.globals[name]) {
            this.globals[name] = [value];
        } else {
            this.globals[name][0] = value;
        }
    },

    rollback_variable: function(name) {
        try {
            // Pop the most recent 
            return this.globals[name].shift();
        } catch(error) {
            throw(
                new RuntimeError(
                    "An internal error occurred; bad rollback."));
        }
    },
    
    // Stubs; these do nothing at the moment.
    disable_side_effects: function() {
        // Disallow calls to functions with side-effects.
        this.side_effects = false;
    },
    
    enable_side_effects: function() {
        this.side_effects = true;
    },
    
    find_first: function(expression) {
        // Find the first row in the table for which the evaluation of the
        // expression tree yields a true result.  This method alters the state
        // of the Context object: the current row will be set to the index of
        // the match, or to the number of rows (final index + 1) if there was
        // no match.
        
        // expression: Valid parse tree representing the expression whose value
        //      is to be tested against every row.
        
        var cell = this.get_current_cell();
        
        // There is no current cell.  It doesn't make sense in this context.
        this.current_cell = null;
        this.disable_side_effects();
        
        for (var i = 0, l = this.table.rows; i < l; ++i) {
            this.set_row(i);
            if (this.evaluate(expression)) {
                break;
            }
        }
        
        if (i == l) {
            // Got to the end of the loop without finding anything.
            throw(Context.NO_MATCH);
        }
        
        this.enable_side_effects();
        this.current_cell = cell;
    },
    
    no_current_cell: function() {
        throw(new RuntimeError("There is no cell."));
    },

    get_current_cell: function() {
        return this.current_cell;
    },
    set_current_cell: function(cell) {
        this.current_cell = cell;
    },
    set_row: function(i) {
        // improve this
        this.row = i;
        if (i > 0) {
            this.optimize_static_subtrees = false;
        }
    },
    get_row: function() {
        return this.row;
    },
    
    save: function() {
        this.saved_row = this.row;
        this.saved_cell = this.current_cell;
    },
    restore: function() {
        this.current_cell = this.saved_cell;
        delete this.saved_cell;
        this.row = this.saved_row;
        delete this.saved_row;
    }
});


Context.Colors = {
    "aqua": 0x00FFFF,
    "gray": 0x808080,
    "navy": 0x000080,
    "silver": 0xC0C0C0,
    "black": 0x000000,
    "green": 0x008000,
    "olive": 0x808000,
    "teal": 0x008080,
    "blue": 0x0000FF,
    "line": 0x00FF00,
    "purple": 0x800080,
    "white": 0xFFFFFF,
    "fuchsia": 0xFF00FF,
    "maroon": 0x800000,
    "red": 0xFF0000,
    "yellow": 0xFFFF00
}

var Evaluator = {
    on_name_change: function(e) {
        Evaluator.change_name($(e.target));
    },
    // do_update: function(e) {
    //     // Change event for the text area.
    //     var textarea = $(e.target),
    //         table = textarea.closest('table'),
    //         th = textarea.closest('th'),
    //         column = table.find('thead th').index(th[0]),
    //         name_input = th.find('input:first');
    //     
    //     if (name_input.val()) {
    //         var name = name_input.val();
    //     } else {
    //         var name = "undefined" + column;
    //         name_input.val(name);
    //         name_input.data('name', name);
    //     }
    //     
    //     if (column == -1) {
    //         return;
    //     }
    //     
    // },
    on_formula_change: function(e) {
        Evaluator.do_update(this, e.formula, e.name_field);
    },

    evaluatextract_float: function(value) {
        return parseFloat(value);
    },

    extract_string: function(value) { 
        return value;
    },
    
    extract_value: function(td, method) {
        // TODO: $extract_value More sophisticated value extraction.
        //
        // Dependents: Array of references to dependent cells.
        var value = $.trim($(td).text()),
            as_float = parseFloat(value);
        if (!isNaN(as_float)) {
            value = as_float;
        }
        return {
            value: value,
            dependents: [],
            element: td
        }
    },

    process_table: function(table) {
        // Given a jQuery object with the table element, construct an object
        // representing its contents, so that we can manipulate it more
        // efficiently in the future.
        table = $(table);
        
        if (table.data("internal")) {
            return table.data("internal");
        }

        var internalized = {},
            column_name = [];

        table.find('th')
            .each(function() {
                var th = $(this),
                    name =
                        th.data('column_name') ||
                        $.trim(th.text());
                if (!name) {
                    return;
                }
                th.data('column_name', th);
                internalized[name] = [];
                column_name.push(name);
            });
        
        var columns = column_name.length;
        
        table.find('td')
            .each(function(i) {
                i %= columns;
                internalized[column_name[i]].push(
                    Evaluator.extract_value(this));
            });
        
        internalized = new Evaluator.TableData(internalized);
        
        internalized.columns = columns;
        internalized.rows = internalized.get_column(column_name[0]).length;
        
        table.data("internal", internalized);

        return internalized;
    }
}

Evaluator.TableData = function(table_data) {
    this.table = table_data;
}

_.extend(Evaluator.TableData.prototype, {
    // Perhaps swap in a SQLite database on enabled browsers here.
    get_column: function(name) {
        return this.table[name];
    },
    
    get_value: function(column, row_num) {
        // return the value in row row_num in column column.
        return this.table[column] && this.table[column][row_num];
    },
    
    change_name: function(name, newname) {
        if (newname in this.table) {
            throw("name is used");
        }
        
        this.table[name] = this.table[current];
        delete this.table[current];
    },
    
    has_column: function(name) {
        return !!(name in this.table);
    },
    
    unused_column_name: function() {
        return "undefined" + this.columns;
    }
});


Evaluator.add_dependent = function(cell, dependent) {
    // Do nothing, for now
    return;

    if (cell.dependents) {
        if (cell.dependents.indexOf(dependent) == -1) {
            // Push the current cell onto the list of its dependents.
            cell.dependents.push(dependent);
        }
    } else {
        cell.dependents = [dependent];
    }
}

Evaluator.DependencyManager = function() {
    
}

$(function() {
    $("table.data")
        .each(function() {
            var table = this;
            setTimeout(function() {
                Evaluator.process_table(table);
            }, 5);
        });
});