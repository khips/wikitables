//
// Parser.js
// Turn a token stream (as outputted by the Tokenizer in token.js)
// into an abstract syntax tree.  This tree can then be used by the evaluator
// in evaluator.js.
//
//
//  TODO: $norecurse Rewrite to avoid using recursion.
tokens = Tokenizer.t;

function Queue(list) {
    this.list = list;
    this.position = 0;
}
_.extend(Queue.prototype, {
    previous: function() {
        return this.list[this.position-1];
    },
    next: function() {
        return this.list[this.position++] || {};
    },
    peek: function(distance) {
        return this.list[this.position + (distance||0)] || {};
    },
    advance: function(n) {
        this.position += (n || 1);
    }
})

/*
 *  The language grammar.  Very simple.
 *      script =
 *          { condition ":" expression { "," expression } }
 *          [ expression { "," expression } ].
 *
 *      list =
 *          "{" [expression | list] { "," (expression | list)} "}" .
 *
 *      call =
 *          ident "(" (expression | list) { "," (expression | list) } ")" .
 *
 *      condition =
 *          expression [(">"|"="|"<") expression ].
 *      
 *      expression = 
 *          ["+"|"-"|"&"] subexpression { ("+"|"-"|"&") subexpression }.
 *
 *      subexpression =
 *          factor { ("*"|"/"|"%") factor } .
 *
 *      factor = ["not"]
 *          call |
 *          ident [":" (number|string)] |
 *          number |
 *          string |
 *          "(" expression ")" .
 */

var Parser = {
    // Lingering question:
    //      Is it better to try, fail, then backtrack or look ahead
    //      occasionally to avoid missteps?  This is a performance 
    //      question, not a philosophical one.

    // Parser.Error
    Error: function(msg, token) {
        this.msg = msg;
        this.token = token;
    },

    // Tree: defined below

    // { 
    //  fn: function, 
    //  children: [],
    //  op: token (used by parse_subexpression and parse_expression)
    // } 

    parse_factor: function(tokens) {
        var next = tokens.peek();

//       if (next.token == tokens.NOT) {
//           tokens.advance();
//           return {
//               fn: FnMap.negate,
//               children: [Parser.parse_factor(tokens)]
//           }
//       }

        if (next.token == tokens.OPEN_PAREN) {
            tokens.advance();
            // return an expression
            var exp = Parser.parse_expression(tokens),
                next = tokens.next();

            if (next.token != tokens.CLOSE_PAREN) {
                throw(
                    new Parser.Error("Expected a close paren."), next);
            }

            return exp;
        } else if (Parser.is_call(tokens)) {
            // return a function call
            return Parser.parse_call(tokens);
        }
        
        // return the literal or identifier
        if (next.token == tokens.IDENT) {
            return tokens.next();
        } else {
            next = tokens.next();
            next.is_static = true;
            return next;
        }
    },

    chain: function(subtree, token) {
        // Translate operators into function calls.  Add additional
        // values as arguments to that function.  If the operator
        // changes, wrap the current generated tree with the function
        // call appropriate to the new operator.

        if (subtree.op != token.token ||
            !subtree.children) {
            subtree = {
                fn: OperatorFn[token.token],
                op: token.token,
                children: [subtree]
            }
        }

        return subtree;
    },

    assert: function(asserted, tokens, error) {
        // TODO: $assert Parser.assert()
    },

    parse_subexpression: function(tokens) {
        var subtree,
            next;

        subtree = Parser.parse_factor(tokens);

        while(true) {
            next = tokens.peek();

            if (next.token == tokens.DIV_OP ||
                next.token == tokens.MUL_OP ||
                next.token == tokens.MOD_OP) {
                // consume the operator:
                tokens.advance();

                subtree = Parser.chain(subtree, next);

                subtree.children.push(
                    Parser.parse_factor(tokens));
            } else {
                break;
            }
        }

        return subtree;
    },
    
    parse_expression: function(tokens) {
        var next = tokens.peek(),
            subtree;

        if (next.token == tokens.SUBT_OP) {
            subtree.fn = FnMap.negative
            // TODO: $negative Replace with a call to negative()
        } else if (next.token == tokens.ADD_OP) {
            // ignore prefix +
            tokens.advance();
        }
        subtree = Parser.parse_subexpression(tokens);

        while(true) {
            next = tokens.peek();

            if (next.token == tokens.ADD_OP ||
                next.token == tokens.SUBT_OP ||
                next.token == tokens.CONCAT_OP) {
                tokens.advance();

                subtree = Parser.chain(subtree, next);

                subtree.children.push(
                    Parser.parse_subexpression(tokens));
            } else {
                break;
            }
        }

        return subtree;
    },

    is_call: function(tokens) {
        // Do the next two tokens construct a call?
        // This is the only case that requires two tokens of
        // lookahead.
        return tokens.peek().token == tokens.IDENT &&
                tokens.peek(1).token == tokens.OPEN_PAREN;
    },

    parse_call: function(tokens) {
        var ident = tokens.next(),
            name = ident.value,
            subtree = {
                fn: FnMap[name],
                children: []
            };

        if (!subtree.fn) {
            // The function did not exist; throw an error.
            throw(
              new Parser.Error(
                  "The function '" + name + "' does not exist.", ident));
        } 
        
        // Skip over open paren:
        tokens.advance();
        
        next = tokens.peek();

        if (next.token == tokens.CLOSE_PAREN) {
            // finish up
            tokens.advance();
            return subtree;
        }

        do {
            if (Parser.is_list(tokens)) {
                subtree.children.push(
                    Parser.parse_list(tokens));
            } else {
                subtree.children.push(
                    Parser.parse_expression(tokens));
            }

            next = tokens.next();

            if (next.token == tokens.CLOSE_PAREN) {
                break;
            }
            
            if (next.token != tokens.ARG_SEPARATOR) {
                // continue to collect arguments
                throw(
                    new Parser.Error(
                        "Function '" + name + "' expected a closing parenthesis.",
                        next || tokens.previous()));
            }
        } while(true);
        
        return subtree;
    },

    is_list: function(tokens) {
        return tokens.peek().token == tokens.OPEN_BRACE;
    },

    parse_list: function(tokens) {
        // skip curly brace
        tokens.advance();
        
        var subtree = [],
            next = tokens.peek();

        subtree.list = true;

        if (next.token == tokens.CLOSE_BRACE) {
            tokens.advance();
            return subtree;
        }
        
        do {
            if (Parser.is_list(tokens)) {
                subtree.push(
                    Parser.parse_list(tokens))
            } else {
                subtree.push(
                    Parser.parse_expression(tokens));
            }

            next = tokens.next();

            if (next.token == tokens.CLOSE_BRACE) {
                break;
            } else if (next.token != tokens.ARG_SEPARATOR) {
                // continue if the next token is an argument
                // separator; otherwise fail.

                throw(
                    new Parser.Error(
                        "Expected a comma or curly brace.", next));
            }
        } while(true);

        return subtree;
    },

    parse_condition: function(tokens) {
        var output = Parser.parse_expression(tokens),
            next;

        while (true) {
            next = tokens.peek();
            
            if (next.token == tokens.GT ||
                next.token == tokens.LT ||
                next.token == tokens.GE ||
                next.token == tokens.LE) {
                
                var fn = OperatorFn[next.token];
                tokens.advance();
            } else {
                break;
            }
            output = {
                fn: fn,
                children: [output, Parser.parse_expression(tokens)]
            }
        }
        
        return output;
    },
    
    parse_script: function(tokens) {
        // The 'tokens' object should implement:
        //  next(), peek(), advance(), and previous() 
        
        if (tokens.peek().token == tokens.BEGIN_CONDITION) {
            tokens.advance();
            var output = {
                fn: FnMap["if"],
                children: [
                    Parser.parse_condition(tokens)
                ]
            };
            tokens.advance(); // dispense END_CONDITION
            output.children.push(Parser.parse_script(tokens));

            if (tokens.peek()) {
                output.children.push(Parser.parse_script(tokens));
            }

            return output;
        };
        
        var output = Parser.parse_expression(tokens);

        while (tokens.peek().token == tokens.ARG_SEPARATOR) {
            if (!output.list) {
                output = [output];
                output.list = true;
            }
            tokens.advance();
            output.push(Parser.parse_expression(tokens));
        }
        
        return output;
    },

    parse: function(tokens) {
        return Parser.parse_script(
            new Queue(tokens));
    },

    parse_str: function(str) {
        return Parser.parse_script(
            new Queue(
                Tokenizer.tokenize(str)));
    },

    mklist: function(iterable) {
        var list = [];
        list.list = true;
        
        if (iterable) {
            for (var i = 0, l = iterable.length; i < l; ++i) {
                list.push(iterable[i]);
            }
        }

        return list;
    }
}

Parser.Error.toString = function() {
    return "Parse Error: " + this.msg + " (at offset " + this.token.offset + ")";
}
