// Resolves properties of Wikipedia entities by pulling their
// corresponding page and scraping the InfoBox.  Yowch.

var InfoBox = {
	// infobox_re: /<table(\s+\w+="[^"]*")*?class="[^"]*infobox[^"]*"(\s+\w+="[^"]*")*>(.*)<\/table>/i,
	infobox_re: /<table\s+(\w+="[^"]*"\s+)*?class="[^"]*infobox[^"]*"(\s+\w+="[^"]*")*>/i,
	table_end: /<\/table>/i,
	fetch_page: function(url) {
		return _.promise(function(promise) {
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = (function() {
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						var text = xhr.responseText;
						var m = text.match(InfoBox.infobox_re);
						text = text.slice(m.index + m[0].length);

						// doesn't really find the matching brace; will
						// correct later:
						m = text.match(InfoBox.table_end);
						text = text.slice(0, m.index);

						var fragment = document.createDocumentFragment();
						var table = document.createElement("table");
						table.innerHTML = text;
						fragment.appendChild(table);

						InfoBox.temp = fragment;
					}
				}
			});
			xhr.open("GET", url, true);
			xhr.send();
		});
	},

	make_resolver_with_link: function(href) {
		// 
	}
};

var DBPedia = {
	fetch_properties: function() {

	}
}


// PREFIX dbo: <http://dbpedia.org/ontology/>
// PREFIX dbpprop: <http://dbpedia.org/property/>

// SELECT * WHERE {
// 	 ?thing foaf:isPrimaryTopicOf <http://en.wikipedia.org/wiki/New_York_City> ;
//          dbpprop:populationTotal ?pop
// }ORDER BY ?name